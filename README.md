Project realized for a Reinforcement Learning class.

Implementation of the UCRL2 algorithm with basic MDP agent, improvement of the 
regular UCRL2 algorithm to take into account bias span constraint.