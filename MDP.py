import numpy as np
from scipy.stats import bernoulli

class MDP(object):
    """
    The MDP class implements a Markov Decision Process simulator 
    """
    
    def __init__(self, P, R):
        """
        ----------
        Parameters

            P :(3D-nparray) Transition matrix where P[state,action,next_state] = probability to reach "next_state" given current "state" and "action"
            R :(2D-nparray) Rewards matrix where R[state,action] = reward obtained by executing "action" when current state is "state"
        ----------
        Returns

            MDP object
        """
        self.P = P
        self.R = R
        self.n_states = self.P.shape[0]
        self.n_actions = self.R.shape[1]

    def step(self, state, action):
        """
        ----------
        Parameters

            state : (int) current state
            action : (int) action to execute
        ----------
        Returns

            reward : (float) obtained reward
            next_state :(int) next state reached
        """
        reward = self.R[state, action]
        prob = self.P[state, action, :]
        next_state = np.argmax(np.random.multinomial(1,prob))
        return reward, next_state

    def collect_traj(self, policy, nb_traj, T):
        """
        ----------
        Parameters

            policy : (1D array) policy to execute
            nb_traj : (int) number of trajectories to generate
            T : (int) Time horizon for one trajectory
        ----------
        Returns

            trajs : (list) Contains list of states, rewards and actions for each trajectory dictionnary.
        """
        trajs = []
        init_state = np.random.randint(self.n_states)
        for n in range(nb_traj):
            rewards = []
            actions= []
            states = [init_state]
            state = init_state
            for t in range(T):
                action = policy[state]
                reward, state = self.step(state, action)
                actions.append(action)
                rewards.append(reward)
                states.append(state)
            print("Trajectory {} simulated".format(n))
            trajs.append({'states':states, 'rewards':rewards, 'actions':actions})
        return trajs

class Policy(object):
    """
    Policy class that can implement stochastic decisions.
    """
    def __init__(self, actions):
        """
        ----------
        Parameters

            actions : (1D array) int when deterministic action, tupple with (action1, action2, prob action1) when stochastic.

        """
        self.actions = actions
    def draw_action(self, state):
        if type(self.actions[state]) != tuple:
            return self.actions[state]
        else:
            action_sup, action_inf, p_action_sup = self.actions[state]
            if bernoulli.rvs(p_action_sup):
                return action_sup
            else:
                return action_inf
