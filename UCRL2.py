import numpy as np
from MDP import *


def inner_max(state, action, sorted_indices, state_values, prob_estimate, upper_prob):
    """
    ----------
    Parameters

        state: (int) state number
        action: (int) action number
        sorted_indices: (1D array) sorted state indices descendingly according to their state value
        state_values: (1D array) state value
        prob_estimate: (3D array) tensor of transition probability estimate
        upper_prob: (2D array) upper confidence bound on L1 norm of transition probability matrix estimate

    ----------
    Returns

        innermax : (float) used to compute value iteration
    """

    indices = sorted_indices.copy()
    p = np.zeros(len(sorted_indices))
    p[indices[0]] = np.min([1, prob_estimate[state,action,indices[0]] + upper_prob[state, action]/2])
    indices.pop(0)
    p[indices] = np.array([prob_estimate[state,action,s] for s in indices])

    l = len(sorted_indices) - 1
    while np.sum(p) > 1:
        p[sorted_indices[l]] = np.max([0, 1 + p[sorted_indices[l]] - np.sum(p)])
        l -= 1

    return np.dot(p, state_values)

def add_bias_span_constraint(state_values, C):
    """
    ----------
    Parameters

        state_values: (1D array) corresponding to the value function
        C: (float) constraint on the bias span


    ----------
    Returns

        state_values : (1D array) original state_values truncated according to the bias constraint criterion.
    """
    m = np.min(state_values)
    return np.array([v if v <= m+C else m+C for v in state_values])

def extended_value_iteration(t, prob_estimate, rewards_estimate, upper_prob, upper_rewards, C=None, show_bias=False):
    """
    ----------
    Parameters

        t: (int) time of iteration
        prob_estimate: (3D array) tensor of transition probability estimate
        rewards_estimate:  (2D array) matrix of rewards estimate
        upper_prob: (2D array) upper confidence bound on L1 norm of transition probability matrix estimate
        upper_rewards: (2D array) upper confidence bound on rewards matrix estimate
        C: (float) constraint on the bias span


    ----------
    Returns

        policy: (1D array) optimistic policy determined on the last value function obtained
        state_values: (1D array) last value function computed
        gain: (float) optimal average reward only valid when 1/sqrt(t) is low enough
    """
    nb_states = prob_estimate.shape[0]
    nb_action = prob_estimate.shape[1]
    state_values = np.zeros(nb_states)
    r_tild = rewards_estimate + upper_rewards
    r_tild_minus = rewards_estimate - upper_rewards

    i = 1
    next_state_values = np.array([np.max([r_tild[state, action] + inner_max(state, action,
                                                                            list(range(nb_states)),
                                                                            state_values, prob_estimate,
                                                                            upper_prob) for action in
                                          range(nb_action)]) for state in range(nb_states)])

    while np.max(next_state_values-state_values) - np.min(next_state_values-state_values) > 1/np.sqrt(t):
        state_values = next_state_values.copy()
        sorted_indices_by_state_value = sorted(range(nb_states), key=lambda x: state_values[x], reverse=True)

        next_state_values = np.array([np.max([r_tild[state, action] + inner_max(state, action, sorted_indices_by_state_value, state_values, prob_estimate, upper_prob) for action in range(nb_action)]) for state in range(nb_states)])
        if C:
            next_state_values = add_bias_span_constraint(next_state_values, C)
        i += 1
    gain = (np.max(next_state_values-state_values) + np.min(next_state_values-state_values)) / 2
    bias = next_state_values - i*gain
    if show_bias:
        print("Bias :", bias)
    state_values = next_state_values
    sorted_indices_by_state_value_dec = sorted(range(nb_states), key=lambda x: state_values[x], reverse=True)
    final_state_values = np.array([np.max([r_tild[state, action] + inner_max(state, action, sorted_indices_by_state_value_dec, state_values, prob_estimate, upper_prob) for action in range(nb_action)]) for state in range(nb_states)])

    # Extracting policy
    if C:
        threshold = np.min(final_state_values) + C
    else:
        threshold = np.inf
    actions = []
    sorted_indices_by_state_value_inc = sorted_indices_by_state_value_dec.copy()
    sorted_indices_by_state_value_inc.reverse()
    for state, value in enumerate(final_state_values):
        if value <= threshold:  # Corresponds to non truncated states
            # In that case the greedy policy
            policy_action = np.argmax([r_tild[state, action] + inner_max(state, action, sorted_indices_by_state_value_dec, state_values, prob_estimate, upper_prob) for action in range(nb_action)])
            actions.append(policy_action)
        else:
            # For those states, policy will be stochastic
            v = [r_tild[state, action] + inner_max(state, action, sorted_indices_by_state_value_dec, state_values, prob_estimate, upper_prob) for action in range(nb_action)]
            action_sup = np.argmax(v)
            v_sup = v[action_sup]

            # The inner min is just inner max with indices sorted in ascending order
            v = [r_tild_minus[state, action] + inner_max(state, action, sorted_indices_by_state_value_inc, state_values, prob_estimate, upper_prob) for action in range(nb_action)]
            action_inf = np.argmin(v)
            v_inf = v[action_inf]

            if v_inf >= threshold:
                actions.append(action_inf)
            else:
                p_action_sup =  (threshold - v_inf) / (v_sup - v_inf)
                actions.append((action_sup, action_inf, p_action_sup))
    if C:
        final_state_values = add_bias_span_constraint(final_state_values, C)
    policy = Policy(actions)
    
    return policy, final_state_values, gain


def UCRL2(nb_steps, d, nb_states, nb_actions,  MDP, C=None):
    """
    ----------
    Parameters

        nb_episodes: (int) number of episodes to generate
        d: (float) confidence coefficient used for upper bounds computation
        nb_states: (int) number of states
        nb_actions: (int) number of actions
        MDP: (MDP object) MDP simulator used to generate trajectories

    ----------
    Returns

        policy: (1D array) optimal policy computed
        rewards_estimate: (2D array) matrix of rewards estimated
        prob_estimate: (3D array) tensor of transition probability estimated
        rewards_history: (1D array) list of rewards
        policy_spans: (1D array) policy spans at each episode
    """

    # Initialisation of parameters
    episode = 1
    state = np.random.randint(nb_states)
    check_state = state
    t = 1
    action = 0

    rewards_episode = np.zeros((nb_states, nb_actions))
    rewards_prior = np.zeros((nb_states, nb_actions))

    # Matrix of counts of visited (s,a) over all prior episodes
    state_action_count_prior = np.zeros((nb_states, nb_actions))
    # Matrix of counts of visited (s,a) over current episode
    state_action_state_count_episode = np.zeros((nb_states, nb_actions, nb_states))
    # Tensor of counts of visited (s,a,s') over all prior episodes
    state_action_state_count_prior = np.zeros((nb_states, nb_actions, nb_states))

    rewards_history = []
    policy_spans = []
    while t <= nb_steps:
        # Updating counts and estimates based on previous episode.
        state_action_count_episode = np.array([[np.sum(s[a]) for a in range(len(s))] for s in state_action_state_count_episode])

        state_action_state_count_prior += state_action_state_count_episode
        state_action_count_prior += state_action_count_episode


        state_action_state_count_episode = np.zeros((nb_states, nb_actions, nb_states))
        state_action_count_episode = np.zeros((nb_states, nb_actions))


        rewards_prior += rewards_episode
        rewards_episode = np.zeros((nb_states, nb_actions))

        # Replacing counts equals to 0 by 1
        sup_1_counts = state_action_count_prior.copy()
        sup_1_counts[sup_1_counts < 1] = 1

        rewards_estimate = rewards_prior/sup_1_counts
        prob_estimate = state_action_state_count_prior / np.tile(sup_1_counts.reshape(nb_states, nb_actions, 1),
                                                                 (1, 1, nb_states))

        upper_reward = np.sqrt(7*np.log(2*nb_states*nb_actions*t/d)/(2*sup_1_counts))
        upper_prob = np.sqrt(14*nb_states*np.log(2*nb_actions*t/d)/(2*sup_1_counts))

        # Computing policy to use for current episode
        policy, state_values, gain = extended_value_iteration(t, prob_estimate, rewards_estimate, upper_prob, upper_reward, C)
        policy_spans.append(np.max(state_values) - np.min(state_values))
        print("Policy obtained at episode {}: ".format(episode), policy.actions, "Timestep : ", t)
        # Simulate an episode using the MDP model and the computed policy
        while state_action_count_episode[check_state, action] < max(1,state_action_count_prior[check_state, action]):
            action = policy.draw_action(state)
            #print(state, action)
            reward, next_state = MDP.step(state, action)
            rewards_episode[state, action] += reward
            state_action_state_count_episode[state, action, next_state] += 1
            state_action_count_episode[state, action] += 1
            check_state = state
            state = next_state
            rewards_history.append(reward)
            t += 1
        episode += 1
    return policy, rewards_estimate, prob_estimate, rewards_history, policy_spans


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    plt.style.use("seaborn-whitegrid")
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')

    # Creating an MDP
    n_states = 3
    n_actions = 2
    delta = 0.001
    P = np.zeros((n_states, n_actions, n_states))
    P[0] = np.array([[0.45, 0.55, 0], [0.3, 0.7, 0]])
    #P[0] = np.array([[1, 0, 0], [1, 0, 0]])
    P[1] = np.array([[delta, 0.8, 0.2-delta], [0, 0, 1]])
    P[2] = np.array([[delta/2, 0.7, 0.3-delta/2], [delta, 0.6, 0.4-delta]])

    R = np.zeros((n_states, n_actions))
    R[0] = np.array([-2, 0])
    R[1] = np.array([-1, 1])
    R[2] = np.array([1, 0])
    testMDP = MDP(P, R)
    print("Running extended value iteration with 0 on uncertainty bounds")
    opt_policy,_, gain = extended_value_iteration(10e12, P, R, np.zeros((n_states, n_actions)), np.zeros((n_states, n_actions)), show_bias=True)
    print("Optimal policy : ", opt_policy.actions)
    print("Gain : ", gain)
    #Test UCRL2 on the MDP
    policy, reward, prob, rewards_history, policy_spans = UCRL2(100000, 0.1, n_states, n_actions, testMDP)
    print("")
    policy_C, reward_C, prob_C, rewards_history_C_const, policy_spans_C = UCRL2(100000, 0.1, n_states, n_actions, testMDP, C=1.5)
    print("Policy obtained : \n", policy.actions)
    print("Policy obtained with C const: \n", policy_C.actions)
    print("Reward obtained : \n", reward)
    print("Prob obtained : \n", prob)

    # Plotting Regret
    regret = np.arange(1,len(rewards_history)+1)*gain - np.cumsum(rewards_history)
    regret_C_const = np.arange(1,len(rewards_history_C_const)+1)*gain - np.cumsum(rewards_history_C_const)
    plt.figure(0)
    plt.plot(regret, label="Regret")
    plt.plot(regret_C_const, label="Regret C constraint")
    plt.xlabel(r"$t$")
    plt.ylabel(r"$R(t)$")
    plt.legend()

    plt.figure(1)
    plt.plot(policy_spans, label="Span of value function")
    plt.plot(policy_spans_C, label="Span of value function with C constraint")
    plt.xlabel("Number of episode")
    plt.legend()
    plt.show()